package com.autoprint.adminpanel;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.HashMap;
import java.util.concurrent.Executor;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        HashMap<String, Object> props = new HashMap<>();
        props.put("server.jetty.max-http-post-size", "100000000");

        new SpringApplicationBuilder()
                .sources(Application.class)
                .properties(props)
                .run(args);
    }

    /**
     * Task executor for asynchronous operations.
     *
     * @see {@link org.springframework.scheduling.annotation.Async}
     */
    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setMaxPoolSize(50);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("srap-async-");
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }
}
