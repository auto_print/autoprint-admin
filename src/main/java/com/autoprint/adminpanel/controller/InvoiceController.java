package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.InvoiceService;
import com.autoprint.adminpanel.services.rest.dto.InvoiceDto;
import com.autoprint.adminpanel.services.rest.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/edit/{id}")
    public String getById(@PathVariable Long id, @RequestParam String orderId, Model model) throws Exception {
        InvoiceDto invoice = invoiceService.getById(id);
        model.addAttribute("invoice", invoice);
        model.addAttribute("orderId", orderId);
        return "invoice/view";
    }

    @PostMapping("/edit/refund/{id}")
    public String refund(@PathVariable Long id, @RequestParam String orderId, RedirectAttributes redirectAttributes, Model model) throws Exception {
        invoiceService.refund(id);
        redirectAttributes.addAttribute("orderId", orderId);
        return "redirect:/admin/invoice/edit/" + id;
    }
}
