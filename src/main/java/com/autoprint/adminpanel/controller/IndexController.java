package com.autoprint.adminpanel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping(value = {"/", "index.html"})
    public String index() {
        return "redirect:/admin";
    }

    @GetMapping(value = {"/admin"})
    public String base() {
        return "index";
    }
}
