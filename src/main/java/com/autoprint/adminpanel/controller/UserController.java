package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.exception.APIConnectionException;
import com.autoprint.adminpanel.services.ActivityService;
import com.autoprint.adminpanel.services.OrderService;
import com.autoprint.adminpanel.services.UserService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admin/user")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private OrderService orderService;

    @GetMapping("/list")
    public String search(
            Model model,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String fullname,
            @RequestParam(required = false, defaultValue = "1") int page) throws Exception{
        List<UserDto> list = userService.list(page, fullname, email);
        model.addAttribute("list", list);
        model.addAttribute("email", email);
        model.addAttribute("fullname", fullname);
        return "user/list";
    }

    @GetMapping("/edit/{id}")
    public String getById(@PathVariable Long id, Model model) throws Exception{
        UserDto user = userService.getById(id);
        List<ActivityDto> activityList = activityService.getByUserId(1, id);
        List<OrderDto> orderList = orderService.getByUserId(id);

        model.addAttribute("user", user);
        model.addAttribute("activities", activityList);
        model.addAttribute("orders", orderList);
        return "user/view";
    }

    @PostMapping("/edit/profile/{id}")
    public String update(
            Model model,
            @PathVariable Long id,
            @RequestParam String fullname,
            @RequestParam String email,
            @RequestParam String phoneNumber) throws Exception {
        failIfBlank(fullname, "fullname");
        failIfBlank(email, "email");
        if (StringUtils.isNotBlank(phoneNumber)) {
            failIfAlphabet(phoneNumber, "phone number");
        }

        UserUpdateDto dto = new UserUpdateDto();
        dto.fullname = fullname;
        dto.email = email;
        dto.phoneNumber = phoneNumber;
        userService.update(id, dto);
        return "redirect:/admin/user/edit/" + id;
    }

    @PostMapping("/edit/{id}/revoke")
    public String revoke(@PathVariable Long id, Model model) throws Exception {
        userService.revoke(id);
        return "redirect:/admin/user/edit/" + id;
    }

    @PostMapping("/edit/{id}/reactivate")
    public String reactivate(@PathVariable Long id, Model model) throws Exception {
        userService.reactivate(id);
        return "redirect:/admin/user/edit/" + id;
    }
}
