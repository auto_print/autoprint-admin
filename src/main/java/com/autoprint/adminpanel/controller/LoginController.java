package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response, HttpSession httpSession, Authentication authentication) {
        return "login";
    }

    @PostMapping("/login/submit")
    public String login(String username, String password) throws Exception {
        if (!authenticationService.login(username, password)) {
            return "redirect:/login?error";
        }
        return "redirect:/admin";
    }
}
