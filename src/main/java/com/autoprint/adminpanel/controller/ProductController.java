package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.ProductVendorService;
import com.autoprint.adminpanel.services.rest.dto.ProductVendorDto;
import com.autoprint.adminpanel.services.rest.dto.ProductVendorUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@Controller
@RequestMapping("/admin/product")
public class ProductController {

    @Autowired
    private ProductVendorService productVendorService;

    @GetMapping("/edit/{id}")
    public String getById(@PathVariable Long id, Model model) throws Exception {
        ProductVendorDto productVendor = productVendorService.getById(id);
        model.addAttribute("productVendor", productVendor);
        return "product/view";
    }

    @PostMapping("/edit/{id}")
    public String update(
            Model model,
            @PathVariable Long id,
            @RequestParam(required = false) Boolean status,
            @RequestParam(required = false) String price) throws Exception {
        ProductVendorUpdateDto dto = new ProductVendorUpdateDto();
        if (price != null) {
            dto.price = new BigDecimal(price);
            if (dto.price.compareTo(BigDecimal.ZERO) < 0) {
                throw new RuntimeException("Price value must be positive!");
            }
        }
        if (status != null) {
            dto.active = status;
        }
        productVendorService.update(id, dto);
        return "redirect:/admin/product/edit/" + id;
    }
}
