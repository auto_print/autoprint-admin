package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.exception.IllegalInputException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.ServletRequestBindingException;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isBlank;

// TODO: uglyyy.... move service
public class BaseController {

    protected void failIfBlank(String value, String args) throws IllegalInputException {
        if (isBlank(value)) {
            String errorMsg = format("%s cannot be blank", args);
            throw new IllegalInputException(errorMsg);
        }
    }

    protected void failIfNull(Object value, String args) throws IllegalInputException {
        if (value == null) {
            throw new IllegalInputException(format("%s cannot be empty", args));
        }
    }

    protected void failIfAlphabet(String value, String args) throws IllegalInputException {
        if (!StringUtils.isNumeric(value)) {
            throw new IllegalInputException(format("%s need to be number", args));
        }
    }


}
