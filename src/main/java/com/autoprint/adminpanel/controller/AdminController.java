package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.AdminService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin/admin")
public class AdminController extends BaseController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/list")
    public String search(
            Model model,
            @RequestParam(required = false, defaultValue = "1") int page) throws Exception {
        List<AdminDto> list = adminService.list(page);
        model.addAttribute("list", list);
        return "admin/list";
    }

    @GetMapping("/add")
    public String add() {
        return "admin/add";
    }

    @GetMapping("/rolesupdate")
    public String rolesupdate(Model model) throws Exception {
        List<AdminRoleDto> listOfRole = adminService.listOfRoles(30);
        model.addAttribute("roles", listOfRole);
        return "admin/roles/update";
    }

    @PostMapping("/create")
    public String create(@RequestParam String username,
                         @RequestParam String password) throws Exception {
        failIfBlank(username, "usernamme");
        failIfBlank(password, "password");
        AdminCreateDto adminCreateDto = new AdminCreateDto();
        adminCreateDto.username = username;
        adminCreateDto.password = password;
        adminService.create(adminCreateDto);
        return "redirect:/admin/admin/list";
    }

    @GetMapping("/createRole")
    public String createRole() {
        return "admin/roles/add";
    }

    @PostMapping("/addRole")
    public String addRole(Model model, @RequestParam String name) throws Exception {
        failIfBlank(name, "name");
        AdminRoleCreateDto adminRoleCreateDto = new AdminRoleCreateDto();
        adminRoleCreateDto.name = name;
        adminService.createRole(adminRoleCreateDto);
        return "redirect:/admin/admin/rolesupdate";
    }

    @PostMapping("/edit/profile/{id}")
    public String update(
            Model model,
            @PathVariable Long id,
            @RequestParam String username) throws Exception {
        failIfBlank(username, "username");
        AdminUpdateDto dto = new AdminUpdateDto();
        dto.username = username;
        adminService.update(id, dto);
        return "redirect:/admin/admin/edit/" + id;
    }

    @GetMapping("/edit/{id}")
    public String getById(@PathVariable Long id, Model model) throws Exception {
        AdminDto admin = adminService.getById(id);
        List<AdminRoleDto> listOfRole = adminService.listOfRoles(30);
        listOfRole = listOfRole.stream().filter(r -> {
            for (AdminRoleDto ar : admin.roles) {
                if (ar.id.equals(r.id))
                    return false;
            }
            return true;
        }).collect(Collectors.toList());
        model.addAttribute("admin", admin);
        model.addAttribute("roles", listOfRole);
        return "admin/view";
    }

    @PostMapping("/edit/{id}/revoke")
    public String revoke(@PathVariable Long id, Model model) throws Exception {
        AdminDto admin = adminService.revoke(id);
        return "redirect:/admin/admin/edit/" + id;
    }

    @PostMapping("/edit/{id}/reactivate")
    public String reactivate(@PathVariable Long id, Model model) throws Exception {
        AdminDto admin = adminService.reactivate(id);
        return "redirect:/admin/admin/edit/" + id;
    }

    @PostMapping("/edit/{id}/role/add")
    public String addRole(@PathVariable Long id, @RequestParam Long roleId, Model model) throws Exception {
        AdminDto admin = adminService.addRole(id, roleId);
        return "redirect:/admin/admin/edit/" + id;
    }

    @PostMapping("/edit/{id}/role/remove")
    public String removeRole(@PathVariable Long id, @RequestParam Long roleId, Model model) throws Exception {
        AdminDto admin = adminService.removeRole(id, roleId);
        return "redirect:/admin/admin/edit/" + id;
    }

    @GetMapping("/roles/edit/{id}")
    public String viewRole(@PathVariable Long id, Model model) throws Exception {
        AdminRoleDto role = adminService.getRoleById(id);
        List<AdminPrivilegeDto> listOfPrivilege = adminService.listOfPrivileges(30);
        listOfPrivilege = listOfPrivilege.stream().filter(r -> {
            for (AdminPrivilegeDto ar : role.privileges) {
                if (ar.id.equals(r.id))
                    return false;
            }
            return true;
        }).collect(Collectors.toList());
        model.addAttribute("role", role);
        model.addAttribute("privileges", listOfPrivilege);
        return "admin/roles/edit";
    }

    @PostMapping("/roles/edit/{roleId}/privilege/add")
    public String addPrivilege(@PathVariable Long roleId, @RequestParam Long privilegeId, Model model) throws Exception {
        AdminRoleDto admin = adminService.addPrivilege(roleId, privilegeId);
        return "redirect:/admin/admin/roles/edit/" + roleId;
    }

    @PostMapping("/roles/edit/{roleId}/privilege/remove")
    public String removePrivilege(@PathVariable Long roleId, @RequestParam Long privilegeId, Model model) throws Exception {
        AdminRoleDto admin = adminService.removePrivilege(roleId, privilegeId);
        return "redirect:/admin/admin/roles/edit/" + roleId;
    }
}
