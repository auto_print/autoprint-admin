package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.StatementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/admin/statement")
public class StatementController {

    @Autowired
    private StatementService statementService;

    @PostMapping("/view/{vendorId}")
    public ResponseEntity<Resource> get(
            @PathVariable Long vendorId,
            @RequestParam String month,
            @RequestParam String year,
            Model model) {
        return statementService.getStatement(vendorId, month, year);
    }

}
