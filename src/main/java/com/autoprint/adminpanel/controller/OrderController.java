package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.OrderService;
import com.autoprint.adminpanel.services.rest.dto.OrderDataDto;
import com.autoprint.adminpanel.services.rest.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admin/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/list")
    public String search(
            Model model,
            @RequestParam(required = false) String referenceId,
            @RequestParam(required = false, defaultValue = "1") int page) throws Exception {
        List<OrderDto> list = orderService.list(page, referenceId);
        OrderDataDto data = orderService.getData();
        //ArrayList<Long> range = orderService.getRange(data.smallestCount, data.biggestCount);
        //model.addAttribute("range", range);
        model.addAttribute("data", data);
        model.addAttribute("list", list);
        model.addAttribute("referenceId", referenceId);
        return "order/list";
    }

    @GetMapping("/edit/{id}")
    public String getById(@PathVariable Long id, Model model) throws Exception {
        OrderDto order = orderService.getById(id);
        model.addAttribute("order", order);
        return "order/view";
    }

    @PostMapping("/edit/{id}/command")
    public String command(@PathVariable Long id, @RequestParam String command, Model model) throws Exception {

        if ("cancel".equalsIgnoreCase(command)) {
            orderService.cancel(id);
        } else if ("reorder".equalsIgnoreCase(command)) {
            OrderDto dto = orderService.reorder(id);
            return "redirect:/admin/order/edit/" + dto.id;
        }

        return "redirect:/admin/order/edit/" + id;
    }
}
