package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.ActivityService;
import com.autoprint.adminpanel.services.rest.dto.ActivityDto;
import com.autoprint.adminpanel.services.rest.dto.ActivityResponseDto;
import com.autoprint.adminpanel.services.rest.dto.ActivityUpdateDto;
import com.autoprint.adminpanel.services.rest.dto.ReportDataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admin/activity")
public class ActivityController extends BaseController {

    @Autowired
    private ActivityService activityService;

    @GetMapping("/list")
    public String search(
            Model model,
            @RequestParam(defaultValue = "false") boolean filter,
            @RequestParam(required = false, defaultValue = "1") int page) throws Exception {
        List<ActivityDto> list = activityService.list(filter, page);
        ReportDataDto data = activityService.getData();
        model.addAttribute("filter", filter);
        model.addAttribute("data", data);
        model.addAttribute("list", list);
        return "activity/list";
    }

    @GetMapping("/edit/{id}")
    public String getById(@PathVariable Long id, Model model,
                          @RequestParam(required = false, defaultValue = "1") int page) throws Exception {
        ActivityDto activity = activityService.getById(id);
        model.addAttribute("activity", activity);
        List<ActivityDto> previous = activityService.getListByCustomId(page, activity.id);
        model.addAttribute("previous", previous);
        return "activity/view";
    }

    @PostMapping("/edit/{id}/response")
    public String response(
            Model model,
            @PathVariable Long id,
            @RequestParam String title,
            @RequestParam String content,
            @RequestParam Long customId,
            @RequestParam Long userId) throws Exception {
        failIfBlank(content, "content");
        ActivityResponseDto dto = new ActivityResponseDto();
        dto.title = title;
        dto.content = content;
        dto.customId = customId;
        dto.userId = userId;
        activityService.response(dto);
        return "redirect:/admin/activity/edit/" + id;
    }

    @PostMapping("/edit/{id}/resolve")
    public String resolve(Model model, @PathVariable Long id) throws Exception {
        ActivityUpdateDto dto = new ActivityUpdateDto();
        dto.resolve = true;
        activityService.update(id, dto);
        return "redirect:/admin/activity/edit/" + id;
    }
}
