package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.PrinterService;
import com.autoprint.adminpanel.services.rest.dto.PrinterDto;
import com.autoprint.adminpanel.services.rest.dto.PrinterUpdateDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/admin/printer")
public class PrinterController extends BaseController {

    @Autowired
    private PrinterService printerService;

    @GetMapping("/edit/{id}")
    public String getById(@PathVariable Long id, Model model, @RequestParam String vendorId) throws Exception {
        PrinterDto printer = printerService.getById(id);
        model.addAttribute("vendorId", vendorId);
        model.addAttribute("printer", printer);
        return "printer/view";
    }

    @PostMapping("/edit/{id}")
    public String update(
            Model model,
            RedirectAttributes redirectAttributes,
            @PathVariable Long id,
            @RequestParam String vendorId,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String prevStatus) throws Exception {
        PrinterUpdateDto dto = new PrinterUpdateDto();

        if (StringUtils.isNotBlank(name)) {
            failIfBlank(name, "Printer name");
            dto.name = name;
        }

        if (prevStatus != null) {
            if (prevStatus.equals(PrinterDto.Status.ACTIVE.toString())) {
                dto.status = PrinterDto.Status.INACTIVE;
            } else if (prevStatus.equals(PrinterDto.Status.INACTIVE.toString())) {
                dto.status = PrinterDto.Status.ACTIVE;
            }
        }
        printerService.update(id, dto);
        redirectAttributes.addAttribute("vendorId", vendorId);
        return "redirect:/admin/printer/edit/" + id;
    }

}
