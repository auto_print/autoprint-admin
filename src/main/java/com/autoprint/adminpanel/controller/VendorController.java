package com.autoprint.adminpanel.controller;

import com.autoprint.adminpanel.services.*;
import com.autoprint.adminpanel.services.rest.VendorRestService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admin/vendor")
public class VendorController extends BaseController {

    @Autowired
    private VendorService vendorService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ProductVendorService productVendorService;
    @Autowired
    private PrinterService printerService;
    @Autowired
    private StatementService statementService;

    @GetMapping("/list")
    public String search(
            Model model,
            @RequestParam(required = false) String email,
            @RequestParam(required = false) String vendorname,
            @RequestParam(required = false, defaultValue = "1") int page) throws Exception {
        List<VendorDto> list = vendorService.list(page, vendorname, email);
        model.addAttribute("list", list);
        model.addAttribute("email", email);
        model.addAttribute("vendorname", vendorname);
        return "vendor/list";
    }

    @GetMapping("/edit/{id}")
    public String getById(@PathVariable Long id, Model model) throws Exception {
        VendorDto vendor = vendorService.getById(id);
        List<OrderDto> orderList = orderService.getByVendorId(id);
        List<PrinterDto> printerList = printerService.getByVendorId(id);
        List<ActivityDto> activityList = activityService.getByUserId(1, vendor.user.id);
        List<ProductVendorDto> productList = productVendorService.getProductVendorList(vendor.id);
        StatementListDto statementListDto = statementService.getStatementList(vendor.id);

        model.addAttribute("vendor", vendor);
        model.addAttribute("orders", orderList);
        model.addAttribute("printerList", printerList);
        model.addAttribute("productList", productList);
        model.addAttribute("activities", activityList);
        model.addAttribute("statementList", statementListDto.statementList);
        return "vendor/view";
    }

    @PostMapping("/edit/profile/{id}")
    public String update(
            Model model,
            @PathVariable Long id,
            @RequestParam String vendorname,
            @RequestParam String email,
            @RequestParam String fullname,
            @RequestParam String phoneNumber) throws Exception {

        failIfBlank(vendorname, "vendorname");
        failIfBlank(fullname, "fullname");
        failIfBlank(email, "email");
        if (StringUtils.isNotBlank(phoneNumber)) {
            failIfAlphabet(phoneNumber, "phone number");
        }

        VendorUpdateDto dto = new VendorUpdateDto();
        dto.vendorname = vendorname;
        dto.email = email;
        dto.fullname = fullname;
        dto.phoneNumber = phoneNumber;
        vendorService.update(id, dto);
        return "redirect:/admin/vendor/edit/" + id;
    }
}
