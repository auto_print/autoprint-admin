package com.autoprint.adminpanel;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value("${app.mode}")
    private String appMode;

    @Value("${app.mode.dev}")
    private String appModeDev;
    @Value("${app.mode.prod}")
    private String appModeProd;
    @Value("${app.mode.uat}")
    private String appModeUat;

    @Value("${app.backend.api.endpoint}")
    private String backendEndpoint;
    @Value("${app.backend.api.token}")
    private String backendToken;

    public String getAppMode() {
        return appMode;
    }

    public String getAppModeDev() {
        return appModeDev;
    }

    public String getAppModeProd() {
        return appModeProd;
    }

    public String getAppModeUat() {
        return appModeUat;
    }

    public String getBackendEndpoint() {
        return backendEndpoint;
    }

    public String getBackendToken() {
        return backendToken;
    }
}
