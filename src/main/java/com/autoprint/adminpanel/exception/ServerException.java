package com.autoprint.adminpanel.exception;

import org.springframework.web.bind.ServletRequestBindingException;

public class ServerException extends ServletRequestBindingException {
    public ServerException(String msg) {
        super(msg);
    }
}
