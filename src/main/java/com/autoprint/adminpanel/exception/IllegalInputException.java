package com.autoprint.adminpanel.exception;

import org.springframework.web.bind.ServletRequestBindingException;

public class IllegalInputException extends ServletRequestBindingException {

    public IllegalInputException(String msg) {
        super(msg);
    }
}
