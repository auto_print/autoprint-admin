package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.exception.APIConnectionException;
import com.autoprint.adminpanel.exception.ServerException;
import com.autoprint.adminpanel.services.rest.UserRestService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRestService userRestService;

    public List<UserDto> list(int pageNumber, String fullname, String email) throws Exception{
        ListDto listDto = getListDto(pageNumber, fullname, email);
        PageUserDto dto = userRestService.getList(listDto);
        return dto.content;
    }

    public UserDto getById(Long id) throws Exception {
        UserDto dto = userRestService.getById(id);
        return dto;
    }

    public void update(Long id, UserUpdateDto dto) throws Exception {
        userRestService.update(id, dto);
    }

    public void reactivate(Long id) throws Exception {
        UserUpdateDto dto = new UserUpdateDto();
        dto.active = true;
        update(id, dto);
    }

    public void revoke(Long id) throws Exception {
        UserUpdateDto dto = new UserUpdateDto();
        dto.active = false;
        update(id, dto);
    }

    private ListDto getListDto(int pageNumber, String fullname, String email) {
        ListDto search = new ListDto();
        search.pageNumber = pageNumber - 1;
        search.pageSize = 30;
        search.sort = "-dateCreated";

        // fixme
        SearchCriteria baseCriteria = new SearchCriteria();
        baseCriteria.setKey("id");
        baseCriteria.setOperation("!:");
        baseCriteria.setValue(1);
        search.searchCriterias.add(baseCriteria);

        // fixme
        /*SearchCriteria baseCriteria_novendor = new SearchCriteria();
        baseCriteria_novendor.setKey("isVendor");
        baseCriteria_novendor.setOperation("!:");
        baseCriteria_novendor.setValue(true);
        search.searchCriterias.add(baseCriteria_novendor);*/

        if (fullname != null && !fullname.isEmpty()) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("fullname");
            criteria.setOperation("~:");
            criteria.setValue(fullname);
            search.searchCriterias.add(criteria);
        }
        if (email != null && !email.isEmpty()) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("email");
            criteria.setOperation("~:");
            criteria.setValue(email);
            search.searchCriterias.add(criteria);
        }
        return search;
    }
}
