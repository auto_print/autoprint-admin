package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.services.rest.InvoiceRestService;
import com.autoprint.adminpanel.services.rest.dto.InvoiceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceService implements BaseService<InvoiceDto> {

    @Autowired
    private InvoiceRestService invoiceRestService;

    @Override
    public InvoiceDto getById(Long id) throws Exception {
        InvoiceDto dto = invoiceRestService.getById(id);
        return dto;
    }

    public void refund(Long id) throws Exception {
        invoiceRestService.refund(id);
    }
}
