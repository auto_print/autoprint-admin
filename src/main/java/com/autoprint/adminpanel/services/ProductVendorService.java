package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.services.rest.ProductVendorRestService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductVendorService implements BaseService<ProductVendorDto> {

    @Autowired
    private ProductVendorRestService productVendorRestService;

    @Override
    public ProductVendorDto getById(Long id) throws Exception {
        ProductVendorDto dto = productVendorRestService.getById(id);
        return dto;
    }

    public void update(Long id, ProductVendorUpdateDto dto) throws Exception {
        productVendorRestService.update(id, dto);
    }

    public List<ProductVendorDto> getProductVendorList(Long vendorId) throws Exception {
        // user dedicated endpoint?
        PageProductVendorDto dto = productVendorRestService.getByVendorId(vendorId);
        return dto.content;
    }
}
