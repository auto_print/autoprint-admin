package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.services.rest.ActivityRestService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityService {

    @Autowired
    private ActivityRestService activityRestService;

    public List<ActivityDto> list(Boolean filter, int pageNumber) throws Exception {
        ListDto listDto = getListDto(pageNumber, "REPORT", filter, null, null);
        PageActivityDto dto = activityRestService.getList(listDto);
        return dto.content;
    }

    public List<ActivityDto> getListByCustomId(int pageNumber, Long customId) throws Exception {
        ListDto listDto = getListDto(pageNumber, "NOTIFICATION", null, customId, null);
        PageActivityDto dto = activityRestService.getList(listDto);
        return dto.content;
    }

    public List<ActivityDto> getByUserId(int pageNumber, Long userId) throws Exception {
        // user dedicated endpoint?
        ListDto listDto = getListDto(pageNumber, "REPORT", null, null, userId);
        PageActivityDto dto = activityRestService.getList(listDto);
        return dto.content;
    }

    public void response(ActivityResponseDto dto) throws Exception {
        activityRestService.response(dto);
    }

    public ActivityDto getById(Long id) throws Exception {
        ActivityDto dto = activityRestService.getById(id);
        return dto;
    }

    public void update(Long id, ActivityUpdateDto dto) throws Exception {
        activityRestService.update(id, dto);
    }


    public ReportDataDto getData() throws Exception {
        return activityRestService.getData();
    }

    private ListDto getListDto(int pageNumber, String type, Boolean filler, Long customId, Long userId) {
        ListDto search = new ListDto();
        search.pageNumber = pageNumber - 1;
        search.pageSize = 30;
        search.sort = "-dateCreated";

        if (StringUtils.isNotBlank(type)) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("type");
            criteria.setOperation(":");
            criteria.setValue(type);
            search.searchCriterias.add(criteria);
        }

        if (customId != null) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("customId");
            criteria.setOperation(":");
            criteria.setValue(customId);
            search.searchCriterias.add(criteria);
        }

        if (userId != null) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("user");
            criteria.setOperation(":");
            criteria.setValue(userId);
            search.searchCriterias.add(criteria);
        }

        if (filler != null && filler) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("resolve");
            criteria.setOperation(":");
            criteria.setValue(false);
            search.searchCriterias.add(criteria);
        }

        return search;
    }
}
