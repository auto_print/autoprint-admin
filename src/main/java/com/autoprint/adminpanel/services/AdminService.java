package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.services.rest.AdminRestService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {

    @Autowired
    private AdminRestService adminRestService;

    public List<AdminDto> list(int pageNumber) throws Exception {
        ListDto listDto = getListDto(pageNumber);
        PageAdminDto dto = adminRestService.getList(listDto);
        return dto.content;
    }

    public List<AdminRoleDto> listOfRoles(int pageNumber) throws Exception {
        ListDto listDto = getListDto(pageNumber);
        PageAdminRoleDto dto = adminRestService.getAllRoles(listDto);
        return dto.content;
    }

    public List<AdminPrivilegeDto> listOfPrivileges(int pageNumber) throws Exception {
        ListDto listDto = getListDto(pageNumber);
        PageAdminPrivilegeDto dto = adminRestService.getAllPrivileges(listDto);
        return dto.content;
    }

    public void create(AdminCreateDto adminCreateDto) throws Exception {
        adminRestService.create(adminCreateDto);
    }

    public void createRole(AdminRoleCreateDto adminRoleCreateDto) throws Exception {
        adminRestService.createRole(adminRoleCreateDto);
    }

    public void update(Long id, AdminUpdateDto adminUpdateDto) throws Exception {
        adminRestService.update(id, adminUpdateDto);
    }

    public AdminDto getById(Long id) throws Exception {
        AdminDto dto = adminRestService.getById(id);
        return dto;
    }

    public AdminDto revoke(Long id) throws Exception {
        AdminDto dto = adminRestService.revoke(id);
        return dto;
    }

    public AdminDto reactivate(Long id) throws Exception {
        AdminDto dto = adminRestService.reactivate(id);
        return dto;
    }

    public AdminRoleDto getRoleById(Long roleId) throws Exception {
        return adminRestService.getRoleById(roleId);
    }

    public AdminDto addRole(Long id, Long roleId) throws Exception {
        AdminDto dto = adminRestService.addRole(id, roleId);
        return dto;
    }

    public AdminDto removeRole(Long id, Long roleId) throws Exception {
        AdminDto dto = adminRestService.removeRole(id, roleId);
        return dto;
    }

    public AdminRoleDto addPrivilege(Long id, Long roleId) throws Exception {
        AdminRoleDto dto = adminRestService.addPrivilege(id, roleId);
        return dto;
    }

    public AdminRoleDto removePrivilege(Long id, Long roleId) throws Exception {
        AdminRoleDto dto = adminRestService.removePrivilege(id, roleId);
        return dto;
    }

    private ListDto getListDto(int pageNumber) {
        ListDto search = new ListDto();
        search.pageNumber = pageNumber - 1;
        search.pageSize = 30;
        search.sort = "-dateCreated";

        return search;
    }

}
