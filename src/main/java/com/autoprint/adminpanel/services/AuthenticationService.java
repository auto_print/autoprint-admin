package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.security.User;
import com.autoprint.adminpanel.services.rest.AdminRestService;
import com.autoprint.adminpanel.services.rest.dto.AdminDto;
import com.autoprint.adminpanel.services.rest.dto.AdminPrivilegeDto;
import com.autoprint.adminpanel.services.rest.dto.AdminRoleDto;
import com.autoprint.adminpanel.services.rest.dto.LoginResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AuthenticationService {

    @Autowired
    private AdminRestService adminRestService;

    String token = "default";

    public Boolean login(String username, String password) throws Exception {
        LoginResponseDto dto = adminRestService.getAdmin(username, password);
        if (dto == null) {
            return false;
        }
        token = dto.token;
        setCurrentUser();
        return true;
    }

    private void setCurrentUser() throws Exception {
        AdminDto adminDto = adminRestService.getAdmin(token);
        // authority
        User user = new User();
        user.setUsername(adminDto.username);

        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();

        for(AdminRoleDto role : adminDto.roles)
        {
            for(AdminPrivilegeDto privilege : role.privileges)
            {
                GrantedAuthority grantedAuthority = new GrantedAuthority() {
                    @Override
                    public String getAuthority() {
                        return privilege.name;
                    }
                };
                grantedAuthorityList.add(grantedAuthority);
            }
        }

        Authentication auth = new UsernamePasswordAuthenticationToken(user, null, grantedAuthorityList);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    public String getToken() {
        return token;
    }
}
