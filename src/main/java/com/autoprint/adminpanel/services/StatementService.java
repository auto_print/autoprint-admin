package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.services.rest.StatementRestService;
import com.autoprint.adminpanel.services.rest.dto.ByteArrayDto;
import com.autoprint.adminpanel.services.rest.dto.StatementListDto;
import com.autoprint.adminpanel.services.rest.dto.StatementRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.ByteArrayInputStream;
import java.util.Base64;

@Service
public class StatementService {

    @Autowired
    private StatementRestService statementOfAccount;

    public StatementListDto getStatementList(Long vendorId) throws Exception {
        return statementOfAccount.getByVendorId(vendorId);
    }

    @ResponseBody
    public ResponseEntity<Resource> getStatement(Long vendorId, String month, String year) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        try {
            StatementRequestDto requestDto = new StatementRequestDto();
            requestDto.vendorId = vendorId;
            requestDto.month = month;
            requestDto.year = year;
            ByteArrayDto dto = statementOfAccount.getStatementOfAccount(requestDto);
            if (dto != null) {
                InputStreamResource resource = new InputStreamResource(new ByteArrayInputStream(Base64.getDecoder().decode(dto.content.getBytes())));
                MediaType type = MediaType.APPLICATION_PDF;
                return ResponseEntity.ok().headers(headers).contentLength(dto.length).contentType(type).body(resource);
            }
        } catch (Exception e) {
        }
        return ResponseEntity.notFound().build();
    }
}
