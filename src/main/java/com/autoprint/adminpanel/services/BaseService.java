package com.autoprint.adminpanel.services;

import java.util.List;

public interface BaseService<T> {

    T getById(Long id) throws Exception;
}
