package com.autoprint.adminpanel.services.rest.dto;

public class ActivityResponseDto {

    public Long customId;
    public Long userId;
    public String title;
    public String content;
}
