package com.autoprint.adminpanel.services.rest.dto;

import java.time.LocalDateTime;

public class OrderOverallObject {

    public LocalDateTime date;
    public String month;
    public Long count;
}
