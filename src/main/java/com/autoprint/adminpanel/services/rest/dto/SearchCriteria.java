package com.autoprint.adminpanel.services.rest.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class SearchCriteria {

    public static final String OPERATOR_EQUALS = ":";
    public static final String OPERATOR_LIKE = "~:";
    public static final String OPERATOR_NOT_EQUALS = "!:";
    public static final String OPERATOR_RANGE = "~";
    public static final String OPERATOR_GREATER = ">";
    public static final String OPERATOR_GREATER_OR_EQUAL = ">=";
    public static final String OPERATOR_LESSER = "<";
    public static final String OPERATOR_LESSER_OR_EQUAL = "<=";

    @NotBlank
    private String key;
    @NotBlank
    private String operation = ":";
    @NotNull
    private Object value;
    @NotNull
    private Boolean conjunction = true;

    public SearchCriteria() {
    }

    public SearchCriteria(String key, String operation, Object value) {
        this.setKey(key);
        this.setOperation(operation);
        this.setValue(value);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Boolean isConjunction() {
        return conjunction;
    }

    public void setConjunction(Boolean conjunction) {
        this.conjunction = conjunction;
    }
}
