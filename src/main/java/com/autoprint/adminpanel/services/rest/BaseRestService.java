package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.AppConfig;
import com.autoprint.adminpanel.exception.APIConnectionException;
import com.autoprint.adminpanel.exception.ServerException;
import com.autoprint.adminpanel.okhttp.KeyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import java.util.ArrayList;
import java.util.Collections;

@Service
public class BaseRestService {

    @Autowired
    private AppConfig appConfig;

    public String getBaseUrl() {
        return appConfig.getBackendEndpoint();
    }

    public ArrayList<KeyValue<String>> defaultHeaderOkHttp() {
        ArrayList<KeyValue<String>> header = new ArrayList<>();
        KeyValue<String> authorization = new KeyValue<>();
        // FIXME: real credential!
        authorization.setKey("Authorization");
        authorization.setValue(appConfig.getBackendToken());
        header.add(authorization);
        return header;
    }

    public HttpHeaders defaultHeaderRestTemplate() {
        return defaultHeaderRestTemplate(appConfig.getBackendToken());
    }

    public HttpHeaders defaultHeaderRestTemplate(String token) {
        HttpHeaders headers = new HttpHeaders();
        // FIXME: real credential!
        headers.set("Authorization", token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }

    public HttpEntity<String> defaultHttpEntity() {
        HttpEntity<String> entity = new HttpEntity<>("", defaultHeaderRestTemplate());
        return entity;
    }

    public HttpEntity<String> defaultHttpEntityWithToken(String token) {
        HttpEntity<String> entity = new HttpEntity<>("", defaultHeaderRestTemplate(token));
        return entity;
    }

    public HttpEntity<Object> defaultHttpEntity(Object requestBody) {
        HttpEntity<Object> entity = new HttpEntity<>(requestBody, defaultHeaderRestTemplate());
        return entity;
    }

    protected void resolveError(Exception e) throws Exception {
        if (e instanceof ResourceAccessException) {
            throw new APIConnectionException();
        } else {
            throw new ServerException(e.getLocalizedMessage());
        }
    }
}
