package com.autoprint.adminpanel.services.rest.dto;

import java.math.BigDecimal;

public class UserDto {
    public Long id;
    public String phoneNumber;
    public String username;
    public String email;
    public String fullname;
    public BigDecimal amount;
    public Boolean active;
}
