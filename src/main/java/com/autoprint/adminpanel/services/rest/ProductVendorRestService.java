package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductVendorRestService extends BaseRestService {

    final String base = "/product/vendor";

    public ProductVendorDto getById(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<ProductVendorDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<ProductVendorDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public PageProductVendorDto getByVendorId(Long vendorId) throws Exception {
        String url = getBaseUrl() + base + "/vendors/" + vendorId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageProductVendorDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<PageProductVendorDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public ProductVendorDto update(Long id, ProductVendorUpdateDto dto) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<ProductVendorDto> responseEntity;
            responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.PUT,
                    defaultHttpEntity(dto),
                    new ParameterizedTypeReference<ProductVendorDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }

        return null;
    }
}
