package com.autoprint.adminpanel.services.rest.dto;

import java.math.BigDecimal;

public class WalletDto {

    public Long id;
    public BigDecimal amount;
}
