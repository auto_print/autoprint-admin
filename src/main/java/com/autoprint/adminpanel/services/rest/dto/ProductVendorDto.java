package com.autoprint.adminpanel.services.rest.dto;

import java.math.BigDecimal;

public class ProductVendorDto {

    public Long id;
    public ProductDto product;
    public VendorDto vendor;
    public BigDecimal price;
    public boolean active;
}
