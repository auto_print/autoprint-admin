package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static org.springframework.http.HttpMethod.GET;

@Service
public class AdminRestService extends BaseRestService {

    private final String base = "/admin";

    public LoginResponseDto getAdmin(String username, String password) throws Exception {
        String url = getBaseUrl() + "/login";

        LoginRequestDto dto = new LoginRequestDto();
        dto.username = username;
        dto.password = password;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<LoginResponseDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(dto),
                    new ParameterizedTypeReference<LoginResponseDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
        }
        return null;
    }

    public AdminDto getAdmin(String token) throws Exception {
        String url = getBaseUrl() + "/admin";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminDto> responseEntity = restTemplate.exchange(
                    url,
                    GET,
                    defaultHttpEntityWithToken(token),
                    new ParameterizedTypeReference<AdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminDto getById(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<AdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminDto create(AdminCreateDto adminCreateDto) throws Exception {
        String url = getBaseUrl() + base;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(adminCreateDto),
                    new ParameterizedTypeReference<AdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }

        return null;
    }

    public AdminRoleDto createRole(AdminRoleCreateDto adminRoleCreateDto) throws Exception {
        String url = getBaseUrl() + base + "/role";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminRoleDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(adminRoleCreateDto),
                    new ParameterizedTypeReference<AdminRoleDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }

        return null;
    }

    public AdminDto update(Long id, AdminUpdateDto adminUpdateDto) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.PUT,
                    defaultHttpEntity(adminUpdateDto),
                    new ParameterizedTypeReference<AdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }

        return null;
    }

    public PageAdminDto getList(ListDto listDto) throws Exception {
        String url = getBaseUrl() + base + "/list";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageAdminDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(listDto),
                    new ParameterizedTypeReference<PageAdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminDto revoke(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id + "/revoke";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<AdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminDto reactivate(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id + "/reactivate";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<AdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public PageAdminRoleDto getAllRoles(ListDto listDto) throws Exception {
        String url = getBaseUrl() + base + "/role/list";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageAdminRoleDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(listDto),
                    new ParameterizedTypeReference<PageAdminRoleDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminDto addRole(Long id, Long roleId) throws Exception {
        String url = getBaseUrl() + base + "/" + id + "/role/" + roleId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<AdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminDto removeRole(Long id, Long roleId) throws Exception {
        String url = getBaseUrl() + base + "/" + id + "/role/" + roleId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.DELETE,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<AdminDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminRoleDto getRoleById(Long roleId) throws Exception {
        String url = getBaseUrl() + base + "/role/" + roleId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminRoleDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<AdminRoleDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public PageAdminPrivilegeDto getAllPrivileges(ListDto listDto) throws Exception {
        String url = getBaseUrl() + base + "/privilege/list";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageAdminPrivilegeDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(listDto),
                    new ParameterizedTypeReference<PageAdminPrivilegeDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminRoleDto addPrivilege(Long roleId, Long privilegeId) throws Exception {
        String base = this.base + "/role";
        String url = getBaseUrl() + base + "/" + roleId + "/privilege/" + privilegeId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminRoleDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<AdminRoleDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public AdminRoleDto removePrivilege(Long roleId, Long privilegeId) throws Exception {
        String base = this.base + "/role";
        String url = getBaseUrl() + base + "/" + roleId + "/privilege/" + privilegeId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<AdminRoleDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.DELETE,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<AdminRoleDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }
}
