package com.autoprint.adminpanel.services.rest.dto;

public class DocumentDto {

    public Long id;
    public String filename;
    public String filetype;
    public Long pages;
    public Long copies;
    public String pageOrientation;
    public String colour;
    public String duplex;
    public String url;
}
