package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.exception.APIConnectionException;
import com.autoprint.adminpanel.exception.ServerException;
import com.autoprint.adminpanel.services.rest.dto.ListDto;
import com.autoprint.adminpanel.services.rest.dto.PageUserDto;
import com.autoprint.adminpanel.services.rest.dto.UserDto;
import com.autoprint.adminpanel.services.rest.dto.UserUpdateDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserRestService extends BaseRestService {

    final String base = "/user";

    public PageUserDto getList(ListDto listDto) throws Exception {
        String url = getBaseUrl() + base + "/list";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageUserDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(listDto),
                    new ParameterizedTypeReference<PageUserDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public UserDto getById(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<UserDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<UserDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public UserDto update(Long id, UserUpdateDto dto) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<UserDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.PUT,
                    defaultHttpEntity(dto),
                    new ParameterizedTypeReference<UserDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }
}
