package com.autoprint.adminpanel.services.rest.dto;

import java.util.List;

public class OrderDataDto {

    public Long totalOrderInMonth;
    public Long totalOrderLastMonth;
    public Long biggestCount;
    public Long smallestCount;
    public List<OrderOverallObject> list;
}
