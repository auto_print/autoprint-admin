package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.okhttp.BuilderHttp;
import com.autoprint.adminpanel.okhttp.OkHttpModel;
import com.autoprint.adminpanel.services.rest.dto.*;
import com.google.gson.Gson;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static com.autoprint.adminpanel.okhttp.EMethod.POST;

@Service
public class OrderRestService extends BaseRestService {

    final String base = "/order";

    public PageOrderDto getList(ListDto listDto) throws Exception {
        String url = getBaseUrl() + base + "/list";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageOrderDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(listDto),
                    new ParameterizedTypeReference<PageOrderDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public OrderDto getById(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<OrderDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<OrderDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public PageOrderDto getByUserId(Long userId) throws Exception {
        String url = getBaseUrl() + base + "/user/" + userId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageOrderDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<PageOrderDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public PageOrderDto getByVendorId(Long vendorId) throws Exception {
        String url = getBaseUrl() + base + "/vendor/" + vendorId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageOrderDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<PageOrderDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }


    public OrderDataDto getData() throws Exception {
        String url = getBaseUrl() + base + "/data";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<OrderDataDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<OrderDataDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public void cancel(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id + "/cancel";

        try {
            BuilderHttp.create()
                    .endpoint(OkHttpModel.create()
                            .requestHeader(defaultHeaderOkHttp())
                            .endpoint(url)
                            .method(POST))
                    .execute();
        } catch (Exception e) {
            resolveError(e);
        }
    }

    public OrderDto reorder(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id + "/reorder";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<OrderDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<OrderDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }

        return null;
    }


}
