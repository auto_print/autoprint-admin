package com.autoprint.adminpanel.services.rest.dto;

public class PrinterUpdateDto {

    public String name;
    public PrinterDto.Status status;
}
