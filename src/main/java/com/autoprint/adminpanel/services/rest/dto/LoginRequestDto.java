package com.autoprint.adminpanel.services.rest.dto;

public class LoginRequestDto {

    public String username;
    public String password;
}
