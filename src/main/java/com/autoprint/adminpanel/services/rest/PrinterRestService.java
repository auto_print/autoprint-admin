package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PrinterRestService extends BaseRestService {

    final String base = "/printer";

    public PrinterDto getById(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PrinterDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<PrinterDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public PagePrinterDto getByVendorId(Long vendorId) throws Exception {
        String url = getBaseUrl() + base + "/vendor/" + vendorId;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PagePrinterDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<PagePrinterDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public PrinterDto update(Long id, PrinterUpdateDto dto) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PrinterDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.PUT,
                    defaultHttpEntity(dto),
                    new ParameterizedTypeReference<PrinterDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }
}
