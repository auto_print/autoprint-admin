package com.autoprint.adminpanel.services.rest.dto;

public class ActivityDto {

    public Long id;
    public Long userId;
    public String title;
    public String content;
    public Type type;
    public Boolean resolve;
    public String referenceId;

    public enum Type {
        NOTIFICATION,
        REPORT
    }

}
