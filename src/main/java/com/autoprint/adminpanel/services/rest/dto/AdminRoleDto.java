package com.autoprint.adminpanel.services.rest.dto;

import java.util.HashSet;
import java.util.Set;

public class AdminRoleDto {

    public Long id;
    public String name;
    public Set<AdminDto> users = new HashSet<>();
    public Set<AdminPrivilegeDto> privileges = new HashSet<>();
}
