package com.autoprint.adminpanel.services.rest.dto;

public class UserUpdateDto {

    public String fullname;
    public String email;
    public String phoneNumber;
    public Boolean active;
}
