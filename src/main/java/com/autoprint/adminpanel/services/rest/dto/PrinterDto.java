package com.autoprint.adminpanel.services.rest.dto;

public class PrinterDto {

    public Long id;
    public String name;
    public Status status;

    public enum Status {
        ACTIVE,
        INACTIVE
    }
}
