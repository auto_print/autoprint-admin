package com.autoprint.adminpanel.services.rest.dto;

import java.util.HashSet;
import java.util.Set;

public class AdminPrivilegeDto {

    public Long id;
    public String name;
    public Set<AdminRoleDto> roles = new HashSet();
}
