package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class VendorRestService extends BaseRestService {

    final String base = "/vendor";

    public PageVendorDto getList(ListDto listDto) throws Exception {
        String url = getBaseUrl() + base + "/list";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageVendorDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(listDto),
                    new ParameterizedTypeReference<PageVendorDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public VendorDto getById(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<VendorDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<VendorDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public VendorDto update(Long id, VendorUpdateDto dto) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<VendorDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.PUT,
                    defaultHttpEntity(dto),
                    new ParameterizedTypeReference<VendorDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }
}
