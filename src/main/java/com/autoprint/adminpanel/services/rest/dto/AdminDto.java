package com.autoprint.adminpanel.services.rest.dto;

import java.util.ArrayList;
import java.util.List;

public class AdminDto {
    public Long id;
    public String username;
    public Status status;
    public String password;
    public String token;
    public List<AdminRoleDto> roles = new ArrayList<>();

    public enum Status {
        ACTIVE,
        INACTIVE
    }

}
