package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.services.rest.dto.ByteArrayDto;
import com.autoprint.adminpanel.services.rest.dto.PageOrderDto;
import com.autoprint.adminpanel.services.rest.dto.StatementListDto;
import com.autoprint.adminpanel.services.rest.dto.StatementRequestDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class StatementRestService extends BaseRestService {

    final String base = "/statement";

    public StatementListDto getByVendorId(Long vendorId) throws Exception {
        String url = getBaseUrl() + base + "/vendor/" + vendorId + "/list";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<StatementListDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<StatementListDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public ByteArrayDto getStatementOfAccount(StatementRequestDto dto) throws Exception {

        String url = getBaseUrl() + base + "/account";
        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<ByteArrayDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(dto),
                    new ParameterizedTypeReference<ByteArrayDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }
}
