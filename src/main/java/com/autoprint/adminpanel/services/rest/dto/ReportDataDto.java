package com.autoprint.adminpanel.services.rest.dto;

public class ReportDataDto {

    public Long totalLastMonthReport;
    public Long totalUnresolveReport;
}
