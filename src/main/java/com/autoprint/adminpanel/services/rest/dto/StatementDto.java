package com.autoprint.adminpanel.services.rest.dto;

public class StatementDto {
    public String month;
    public String year;
}
