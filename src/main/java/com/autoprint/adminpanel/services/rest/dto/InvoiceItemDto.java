package com.autoprint.adminpanel.services.rest.dto;

import java.math.BigDecimal;

public class InvoiceItemDto {
    public Long id;
    public OrderDto order;
    public ProductDto product;
    public BigDecimal amount;
}
