package com.autoprint.adminpanel.services.rest.dto;

public class LoginResponseDto {

    public Long id;
    public String token;
    public String type;
}
