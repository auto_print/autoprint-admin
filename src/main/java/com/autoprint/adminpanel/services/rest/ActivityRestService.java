package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.exception.ServerException;
import com.autoprint.adminpanel.okhttp.BuilderHttp;
import com.autoprint.adminpanel.okhttp.OkHttpModel;
import com.autoprint.adminpanel.okhttp.OkHttpResponse;
import com.autoprint.adminpanel.services.rest.dto.*;
import com.google.gson.Gson;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static com.autoprint.adminpanel.okhttp.EMethod.POST;

@Service
public class ActivityRestService extends BaseRestService {

    final String base = "/activity";

    public PageActivityDto getList(ListDto listDto) throws Exception {
        String url = getBaseUrl() + base + "/list";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<PageActivityDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(listDto),
                    new ParameterizedTypeReference<PageActivityDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public ActivityDto getById(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<ActivityDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<ActivityDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public ActivityDto response(ActivityResponseDto dto) throws Exception {
        String url = getBaseUrl() + base + "/response";
        try {
            BuilderHttp.create()
                    .endpoint(OkHttpModel.create()
                            .requestHeader(defaultHeaderOkHttp())
                            .requestEntity(new Gson().toJson(dto))
                            .endpoint(url)
                            .method(POST))
                    .execute();
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public ActivityDto update(Long id, ActivityUpdateDto dto) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<ActivityDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.PUT,
                    defaultHttpEntity(dto),
                    new ParameterizedTypeReference<ActivityDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public ReportDataDto getData() throws Exception {
        String url = getBaseUrl() + base + "/data";

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<ReportDataDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<ReportDataDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }
}
