package com.autoprint.adminpanel.services.rest.dto;

public class VendorDto {
    public Long id;
    public String vendorname;
    public Double longitude;
    public Double latitude;
    public UserDto user;
}
