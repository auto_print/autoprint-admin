package com.autoprint.adminpanel.services.rest.dto;

import java.math.BigDecimal;
import java.util.List;

public class InvoiceDto {

    public Long id;
    public String referenceId;
    public Status status;
    public BigDecimal amount;
    public List<InvoiceItemDto> invoiceItems;

    public enum Status {
        PAID,
        UNPAID,
        CANCELLED,
        REFUND
    }
}
