package com.autoprint.adminpanel.services.rest.dto;

public class StatementRequestDto {

    public Long vendorId;
    public String month;
    public String year;
}
