package com.autoprint.adminpanel.services.rest.dto;

import java.time.LocalDateTime;
import java.util.List;

public class OrderDto {

    public Long id;
    public Long userId;
    public String referenceId;
    public Status status;
    public List<InvoiceDto> invoices;
    public LocalDateTime dateCreated;

    public enum Status {
        PENDING,
        CONFIRM,
        CANCELLED,
        COMPLETED,
        REJECTED
    }
}
