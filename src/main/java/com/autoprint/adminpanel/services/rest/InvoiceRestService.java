package com.autoprint.adminpanel.services.rest;

import com.autoprint.adminpanel.services.rest.dto.InvoiceDto;
import com.autoprint.adminpanel.services.rest.dto.OrderDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class InvoiceRestService extends BaseRestService {

    final String base = "/invoice";

    public InvoiceDto getById(Long id) throws Exception {
        String url = getBaseUrl() + base + "/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<InvoiceDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<InvoiceDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }

    public InvoiceDto refund(Long id) throws Exception {
        String url = getBaseUrl() + base + "/refund/" + id;

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<InvoiceDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.POST,
                    defaultHttpEntity(),
                    new ParameterizedTypeReference<InvoiceDto>() {
                    });
            if (HttpStatus.OK.equals(responseEntity.getStatusCode())) {
                return responseEntity.getBody();
            }
        } catch (Exception e) {
            resolveError(e);
        }
        return null;
    }
}
