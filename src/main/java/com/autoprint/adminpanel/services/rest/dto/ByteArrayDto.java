package com.autoprint.adminpanel.services.rest.dto;

public class ByteArrayDto {

    public String content;
    public Integer length;
    public String contentType = "application/pdf";
}
