package com.autoprint.adminpanel.services.rest.dto;

public class VendorUpdateDto {

    public String vendorname;
    public String phoneNumber;
    public String fullname;
    public String email;
}
