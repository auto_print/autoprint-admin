package com.autoprint.adminpanel.services.rest.dto;

import java.math.BigDecimal;

public class ProductVendorUpdateDto {

    public BigDecimal price;
    public Boolean active;
}
