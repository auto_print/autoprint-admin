package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.services.rest.VendorRestService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VendorService {

    @Autowired
    private VendorRestService vendorRestService;

    public List<VendorDto> list(int pageNumber, String vendorname, String email) throws Exception {
        ListDto listDto = getListDto(pageNumber, vendorname, email);
        PageVendorDto dto = vendorRestService.getList(listDto);
        return dto.content;
    }

    public VendorDto getById(Long id) throws Exception {
        VendorDto dto = vendorRestService.getById(id);
        return dto;
    }

    public void update(Long id, VendorUpdateDto dto) throws Exception {
        vendorRestService.update(id, dto);
    }

    private ListDto getListDto(int pageNumber, String vendorname, String email) {
        ListDto search = new ListDto();
        search.pageNumber = pageNumber - 1;
        search.pageSize = 30;
        search.sort = "-dateCreated";

        if (vendorname != null && !vendorname.isEmpty()) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("vendorname");
            criteria.setOperation("~:");
            criteria.setValue(vendorname);
            search.searchCriterias.add(criteria);
        }
        if (email != null && !email.isEmpty()) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("email");
            criteria.setOperation("~:");
            criteria.setValue(email);
            search.searchCriterias.add(criteria);
        }
        return search;
    }
}
