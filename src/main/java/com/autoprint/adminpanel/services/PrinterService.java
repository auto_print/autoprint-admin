package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.services.rest.PrinterRestService;
import com.autoprint.adminpanel.services.rest.dto.PagePrinterDto;
import com.autoprint.adminpanel.services.rest.dto.PrinterDto;
import com.autoprint.adminpanel.services.rest.dto.PrinterUpdateDto;
import com.autoprint.adminpanel.services.rest.dto.VendorUpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrinterService implements BaseService<PrinterDto> {

    @Autowired
    private PrinterRestService printerRestService;

    public PrinterDto getById(Long id) throws Exception {
        return printerRestService.getById(id);
    }

    public List<PrinterDto> getByVendorId(Long vendorId) throws Exception {
        PagePrinterDto pagePrinterDto = printerRestService.getByVendorId(vendorId);
        return pagePrinterDto.content;
    }

    public void update(Long id, PrinterUpdateDto dto) throws Exception {
        printerRestService.update(id, dto);
    }
}
