package com.autoprint.adminpanel.services;

import com.autoprint.adminpanel.services.rest.OrderRestService;
import com.autoprint.adminpanel.services.rest.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService implements BaseService<OrderDto> {

    @Autowired
    private OrderRestService orderRestService;

    public List<OrderDto> list(int pageNumber, String referenceId) throws Exception {
        ListDto listDto = getListDto(pageNumber, referenceId);
        PageOrderDto dto = orderRestService.getList(listDto);
        return dto.content;
    }

    @Override
    public OrderDto getById(Long id) throws Exception {
        OrderDto dto = orderRestService.getById(id);
        return dto;
    }

    public List<OrderDto> getByUserId(Long userId) throws Exception {
        PageOrderDto dto = orderRestService.getByUserId(userId);
        return dto.content;
    }

    public List<OrderDto> getByVendorId(Long vendorId) throws Exception {
        PageOrderDto dto = orderRestService.getByVendorId(vendorId);
        return dto.content;
    }

    public OrderDataDto getData() throws Exception {
        return orderRestService.getData();
    }

    public void cancel(Long id) throws Exception {
        orderRestService.cancel(id);
    }

    public OrderDto reorder(Long id) throws Exception {
        return orderRestService.reorder(id);
    }

    public ArrayList<Long> getRange(Long smallest, Long biggest) {
        ArrayList<Long> range = new ArrayList<>();

        /*if (biggest == smallest) {
            // no range
            range.add(biggest);
            return range;
        }*/

        if (biggest - smallest < 4) {
            for (Long i = smallest; i < biggest; i++) {
                range.add(i);
            }
            return range;
        }

        Long diff = biggest - smallest;
        Long gap = diff / 4;

        for (Long i = smallest; i < biggest; i += gap) {
            range.add(i);
        }
        return range;
    }

    private ListDto getListDto(int pageNumber, String referenceId) {
        ListDto search = new ListDto();
        search.pageNumber = pageNumber - 1;
        search.pageSize = 30;
        search.sort = "-dateCreated";

        if (referenceId != null && !referenceId.isEmpty()) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setKey("referenceId");
            criteria.setOperation(":");
            criteria.setValue(referenceId);
            search.searchCriterias.add(criteria);
        }
        return search;
    }
}
