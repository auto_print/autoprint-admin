package com.autoprint.adminpanel.okhttp;

import okhttp3.Response;

import java.io.IOException;

public class OkHttpResponse {
    private String body;
    private int responseCode;
    private boolean successful;

    public OkHttpResponse(String body, int responseCode, boolean successful) {
        this.body = body;
        this.responseCode = responseCode;
        this.successful = successful;
    }

    public static OkHttpResponse builder(Response response) throws IOException {
        return new OkHttpResponse(response.body().string(), response.code(), response.isSuccessful());
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
}
