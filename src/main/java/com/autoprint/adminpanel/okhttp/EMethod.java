package com.autoprint.adminpanel.okhttp;

public enum EMethod {
    GET, POST, PUT, DELETE
}
