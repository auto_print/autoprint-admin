package com.autoprint.adminpanel.security;

import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;

import static org.apache.commons.lang3.StringUtils.isBlank;

public class UsernamePasswordAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        // TODO:
        return "basic";
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        UsernamePassword usernamePassword = new UsernamePassword();
        usernamePassword.setUsername(request.getParameter("username"));
        usernamePassword.setPassword(request.getParameter("password"));

        if (isBlank(usernamePassword.getUsername()) || isBlank(usernamePassword.getPassword())) {
            return null;
        }

        return usernamePassword;
    }
}
