package com.autoprint.adminpanel.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.Collections;

import static org.springframework.security.config.http.SessionCreationPolicy.ALWAYS;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class HttpSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CorsConfig corsConfig;
    @Autowired
    private CustomUserDetailsService userDetailsService;

//    @Bean
//    public SessionAuthenticationFilter sessionAuthenticationFilter() {
//        SessionAuthenticationFilter preAuthenticationFilter = new SessionAuthenticationFilter();
//        preAuthenticationFilter.setAuthenticationManager(authenticationManager());
//        return preAuthenticationFilter;
//    }

    @Bean
    public UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter() {
        UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter = new UsernamePasswordAuthenticationFilter();
        usernamePasswordAuthenticationFilter.setAuthenticationManager(authenticationManager());
        return usernamePasswordAuthenticationFilter;
    }

    @Bean
    public AuthenticationProvider authorizationProvider() {
        PreAuthenticatedAuthenticationProvider authenticationProvider = new PreAuthenticatedAuthenticationProvider();
        authenticationProvider.setPreAuthenticatedUserDetailsService(userDetailsService);
        authenticationProvider.setThrowExceptionWhenTokenRejected(false);
        return authenticationProvider;
    }

    @Override
    protected AuthenticationManager authenticationManager() {
        ProviderManager providerManager = new ProviderManager(Collections.singletonList(authorizationProvider()));
        // Don't erase cred after auth-ed
        providerManager.setEraseCredentialsAfterAuthentication(false);
        return providerManager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement()
                .sessionCreationPolicy(ALWAYS)
                .sessionFixation().migrateSession()
                .and()
                // TODO: enable CSRF
                .csrf().disable()
                //.cors().configurationSource(corsConfig.corsConfigurationSource())
                //.and()
                .exceptionHandling()
                .authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login"))
                .and()
                .addFilter(usernamePasswordAuthenticationFilter())
                // Let spring do the magic
                //.addFilter(sessionAuthenticationFilter())
                .authorizeRequests()
                .antMatchers("/admin/**").authenticated()
                .antMatchers("/js/**",
                        "/css/**",
                        "/img/**",
                        "/vendor/**").permitAll()
                .anyRequest().permitAll()
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout")
                .permitAll();
    }
}
