package com.autoprint.adminpanel.security;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
public class CustomUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        if (token.getCredentials() instanceof UsernamePassword) {
            // We expect an array of 2 elements
            return loadByUsernamePassword((UsernamePassword) token.getCredentials());
        } else {
            throw new BadCredentialsException(format("Unexpected credential type '%s'", token.getCredentials().getClass().getSimpleName()));
        }
    }


    private UserDetails loadByUsernamePassword(UsernamePassword usernamePassword) {
        return null;
    }
}
