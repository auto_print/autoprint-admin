package com.autoprint.adminpanel.security;

import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
public class CorsConfig {

    @Value("${app.security.cors.allowed_origins}")
    private String allowedOrigins;

    public CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", createConfig());
        return source;
    }

    private CorsConfiguration createConfig() {
        String[] allowOrigins = getAllowedOrigins();

        // Allow any origin by default
        // TODO: maybe not a good idea to allow all
        if (allowOrigins.length == 1 && "".equals(allowOrigins[0])) {
            allowOrigins = new String[]{"*"};
        }

        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(ImmutableList.copyOf(allowOrigins));
        configuration.setAllowedMethods(ImmutableList.of("*"));
        configuration.setAllowedHeaders(ImmutableList.of("*"));
        return configuration;
    }

    public String[] getAllowedOrigins() {
        assert allowedOrigins != null;
        return allowedOrigins.split(",");
    }
}
