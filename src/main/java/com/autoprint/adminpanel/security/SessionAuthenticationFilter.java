package com.autoprint.adminpanel.security;

import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;

public class SessionAuthenticationFilter extends AbstractPreAuthenticatedProcessingFilter {

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        // TODO:
        return "basic";
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        // Play-safe, tho we know session won't be null as session is always created if none exist
        if (request.getSession() == null) {
            return null;
        }

        // Make sure username and token cookies exist
        Object userObject = request.getSession().getAttribute("user");
        if (userObject == null) {
            return null;
        }

        return userObject;
    }
}
